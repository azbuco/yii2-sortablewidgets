<?php

namespace azbuco\sortablewidgets;

use Closure;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

trait SortableTrait
{

    /**
     * @var array the options for the Sortable plugin
     * @see https://github.com/RubaXa/Sortable
     */
    public $clientOptions = [];

    /**
     * @var false|string|Closure URL to handle the sort. Used within the sortable's
     * default onEnd method.
     */
    public $sortUrl = null;

    /**
     * @var string the POST property name for sortUrl
     */
    public $sortQueryVar = 'sort';

    /**
     * @var boolean if false, the defaultClientOptions ignored
     */
    public $useDefaults = true;

    public function setDefaults()
    {
        if (!$this->useDefaults) {
            return;
        }

        $sortUrl = $this->sortUrl instanceof Closure ? $this->sortUrl() : $this->sortUrl;

        if ($sortUrl && !isset($this->clientOptions[''])) {
            $this->defaultClientOptions['onEnd'] = new JsExpression("function (e) {
                $.post('$sortUrl', {
                {$this->sortQueryVar}: this.toArray()
                }).fail(function() {
                    alert('Unable to save the new order.');
                });
            }");
        }

        $this->clientOptions = ArrayHelper::merge($this->defaultClientOptions, $this->clientOptions);
    }

    public function registerBundle()
    {
        RubaxaSortableAsset::register(Yii::$app->view);
        SortableAsset::register(Yii::$app->view);
    }

}
