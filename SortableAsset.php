<?php

namespace azbuco\sortablewidgets;

use yii\web\AssetBundle;

class SortableAsset extends AssetBundle {

    public $sourcePath = '@azbuco/sortablewidgets/assets';
    public $css = [
        'css/sortable-widgets.css',
    ];
    public $js = [
        'js/jquery.binding.js',
        'js/sortable-widgets.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'azbuco\sortablewidgets\RubaxaSortableAsset',
    ];

}
