# Yii2 sortable widgets #

## Usage ##

### ListView or Grid sortable column ###

```
<?=
SortableListView::widget([
    // normal ListView configuration
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => function ($model, $key, $index, $widget) {
        return '<span class="sortable-widget-handle" data-key="' . $model->id . '">&#9776;</span> ' . $model->id;
    },

    // Url to send sort data. Leave empty if you handle it yourself.
    'sortUrl' => Url::to(['sort']),

    // see https://github.com/RubaXa/Sortable
    // and defaultOptions in SortableListView file
    'clientOptions' => ['handle' => '.sortable-widget-handle'],
]);
?>
```

Or make a sortable grid column

```
<?php
// In GridView's column config
'columns' => [
    [
        'class' => SortableColumn::className(),
        // Url to send sort data. Leave empty if you handle it yourself.
        'sortUrl' => Url::to(['sort']),
    ],
// ...
?>

```

The sort action that handles List / Grid update

```
<?php 
// in the Controller
public function actions()
{
    return [
        // ...
        'sort' => [
            'class' => \azbuco\sortablewidgets\SortableAction::className(),
            'modelClass' => ModelClass::className(),
            'sortProperty' => 'sort_property_name',
        ],
    ];
}

```

### Sortable Input Widget ###

```
<?php 
    // in this case the model must handle the attributes as array
    echo $form->field($model, 'attribute')->widget(\azbuco\sortablewidgets\SortableSelect::className(), [
    'items' => [
        // all avialable items
        'foo' => 'FOO',
        'bar' => 'BAR',
    ],
])?>

```

### composer.json ###

```
#!javascript
// ...
"require": {
    // ...
    "azbuco/yii2-sortablewidgets": "dev-master"
},
"extra": {
    // ...
    "asset-repositories": [
        // ...
        {
            // clarify, because there are a sortable asset on bower with same name but lowercase.
            "name": "bower-asset/Sortable", 
            "type": "bower-vcs",
            "url": "https://github.com/RubaXa/Sortable.git"
        }
    ]
},
"repositories": [
    // ...
    {
        "type": "vcs",
        "url": "git@bitbucket.org:azbuco/yii2-sortablewidgets.git"
    }
]
```