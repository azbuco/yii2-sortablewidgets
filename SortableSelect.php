<?php

namespace azbuco\sortablewidgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class SortableSelect extends InputWidget
{
    /**
     * @var type array $selection the selected values.
     */
    public $selection = [];

    /**
     * @var array the option data items. The array keys are option values, and the array values
     * are the corresponding option labels. 
     */
    public $items = [];

    /**
     * @var array additional parameters to be passed to [[itemView]] when it is being rendered.
     * This property is used only when [[itemView]] is a string representing a view name.
     */
    public $viewParams = [];

    /**
     * @var string view for the widget. Avialable params: $inline $selected $avialable.
     */
    public $contentView = '@azbuco/sortablewidgets/views/modal';

    /**
     * @var array SortableSelect api configuration
     */
    public $clientOptions = [];

    /**
     * @var array default configuration for sortable
     * $see https://github.com/RubaXa/Sortable
     * 
     */
    public $defaultSortableOptions = [
        'animation' => 100,
        'draggable' => '.sortable-item',
        'dataIdAttr' => 'data-key',
        'handle' => '.sortable-handle',
    ];

    /**
     * @var array the options for the Sortable plugin
     * @see https://github.com/RubaXa/Sortable
     */
    public $sortableOptions = [];

    /**
     * @var string|null title for the modal dialog, if null, it shows the default title
     * 
     */
    public $modalTitle = null;
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (isset($this->options['class'])) {
            $this->options['class'] .= ' sortable-widget sortable-select';
        } else {
            $this->options['class'] = 'sortable-widget sortable-select';
        }

        $this->clientOptions['sortableOptions'] = ArrayHelper::merge($this->defaultSortableOptions, $this->sortableOptions);
        if (!array_key_exists('group', $this->clientOptions['sortableOptions'])) {
            $this->clientOptions['sortableOptions']['group'] = $this->getId();
        }

        // scripts
        $this->registerBundle();
        $this->registerClientScript();
        $this->registerTranslations();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->options['id'] = $this->getId();

        $inline = $this->renderInline();
        $selected = $this->renderSelected();
        $avialable = $this->renderAvialable();

        $content = $this->renderInput();
        $content .= $this->render($this->contentView, [
            'inline' => $inline,
            'selected' => $selected,
            'avialable' => $avialable,
            'widget' => $this,
            'title' => $this->modalTitle,
        ]);

        echo Html::tag('div', $content, $this->options);
    }

    public function renderInput()
    {
        $options = [
            'data-role' => 'sortable-select-input',
            'multiple' => true,
        ];

        if ($this->hasModel()) {
            $selected = [];
            $avialable = $this->items;

            $items = $this->model->{$this->attribute};
            if (empty($items)) {
                $items = [];
            }

            foreach ($items as $key) {
                $selected[$key] = $avialable[$key];
                unset($avialable[$key]);
            }

            $items = $selected + $avialable;

            return Html::activeDropDownList($this->model, $this->attribute, $items, $options);
        } else {
            return Html::dropDownList($this->name, $this->selection, $this->items, $options);
        }
    }

    public function renderSelected()
    {
        return Html::tag('div', '', ['data-role' => 'sortable-select-selected']);
    }

    public function renderAvialable()
    {
        return Html::tag('div', '', ['data-role' => 'sortable-select-avialable']);
    }

    public function renderInline()
    {
        return Html::input('text', null, null, ['data-role' => 'sortable-select-inline', 'class' => 'form-control']);
    }

    public function registerClientScript()
    {
        $id = $this->id;
        $options = Json::encode($this->clientOptions);
        $js = "; SortableSelect('#$id', $options);\n";
        $this->view->registerJs($js);
    }

    public function registerBundle()
    {
        RubaxaSortableAsset::register(Yii::$app->view);
        SortableAsset::register(Yii::$app->view);
    }

    public function registerTranslations()
    {

        Yii::$app->i18n->translations['sortablewidgets*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
//            'sourceLanguage' => 'en-US',
            'basePath' => '@azbuco/sortablewidgets/messages',
//            'fileMap' => [
//                'azbuco/sortablewidgets/base' => 'base.php',
//            ],
        ];
    }
}
