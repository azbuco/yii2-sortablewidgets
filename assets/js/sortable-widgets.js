var SortableSelect = function (el, options) {

    var api = {};

    api.defaults = {
        sortableOptions: {
            
        },
        renderItem: function(option) {
            return $('<div class="sortable-item"><span class="sortable-handle">&#9776;</span> </div>')
                    .append(option.text())
                    .attr('data-key', option.attr('value'));
        }
    };

    var s = api.settings = $.extend({}, api.defaults, options);
    
    api.el = el instanceof jQuery ? el : $(el);

    api.input = api.el.find('[data-role="sortable-select-input"]');
    api.inline = api.el.find('[data-role="sortable-select-inline"]');
    api.selected = api.el.find('[data-role="sortable-select-selected"]');
    api.avialable = api.el.find('[data-role="sortable-select-avialable"]');
    
    api.update = function() {
        $.each(api.selected.find('.sortable-item'), function() {
            var option = api.input.find('option[value="' + $(this).attr('data-key') + '"]');
            if (option.length > 0) {
                option.prop('selected', true);
                api.input.append(option);
            }
        });
        
        $.each(api.avialable.find('.sortable-item'), function() {
            var option = api.input.find('option[value="' + $(this).attr('data-key') + '"]');
            if (option.length > 0) {
                option.prop('selected', false);
                api.input.append(option);
            }
        });
        
        if (api.inline.length > 0) {
            var content = [];
            $.each(api.input.find('option:selected'), function() {
                content.push($(this).text());
            });
            api.inline.val(content.join(', '));
        }
        
        api.input.trigger('change');
    };
    
    var init = function () {
        $.each(api.input.find('option'), function() {
            var option = $(this);
            var item = s.renderItem(option);
            if (option.is(':selected')) {
                api.selected.append(item);
            } else {
                api.avialable.append(item);
            }
        });
        
        api.avialable.sortable(s.sortableOptions);
        api.selected.sortable(s.sortableOptions);
        
        api.update();
    };

    init();
    
    var events = function() {
        $(document).on('sort', api.el, function() {
            api.update();
        });
    };
    
    events();
    
    
    
    return api;
};
