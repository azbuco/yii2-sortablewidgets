<?php

namespace azbuco\sortablewidgets;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class SortableAction extends Action
{

    public $sortQueryVar = 'sort';
    public $sortProperty = 'sort';
    public $modelClass;
    
    public $successStatusCode = 204;

    public function run()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            foreach (Yii::$app->request->post($this->sortQueryVar) as $index => $id) {
                $class = $this->modelClass;
                $model = $class::findOne($id);
                /* @var $model ActiveRecord */
                if ($model === null) {
                    $transaction->rollBack();
                    throw new NotFoundHttpException();
                }
                $model->{$this->sortProperty} = $index;
                $model->update(false, [$this->sortProperty]);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ServerErrorHttpException($e->getMessage());
        }

        $transaction->commit();
        Yii::$app->response->statusCode = $this->successStatusCode;
    }

}
