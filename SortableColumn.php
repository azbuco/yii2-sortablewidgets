<?php

namespace azbuco\sortablewidgets;

use yii\grid\Column;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

class SortableColumn extends Column
{

    use SortableTrait;

    /**
     * @var array default configuration for sortable
     * $see https://github.com/RubaXa/Sortable
     * 
     */
    public $defaultClientOptions = [
        'animation' => 100,
        'dataIdAttr' => 'data-key',
        'handle' => '.sortable-widget-handle',
    ];
    
    public $headerOptions = [
        'class' => 'sortable-column',
    ];
    
    public $contentOptions = [
        'class' => 'sortable-column',
    ];

    public function init()
    {        
        if (isset($this->grid->options['class'])) {
            $this->grid->options['class'] .= ' sortable-widget';
        } else {
            $this->grid->options['class'] = 'sortable-widget';
        }
        
        if ($this->content === null) {
            $this->content = function($model, $key, $index, $column) {
                return Html::tag('div', '&#9776;', [
                            'class' => 'sortable-widget-handle',
                            'data-key' => $model->id,
                ]);
            };
        }

        // defaults
        $this->setDefaults();

        $this->registerBundle();
        $this->registerClientScript();
    }

    public function registerClientScript()
    {
        $id = $this->grid->id;
        $options = Json::encode(ArrayHelper::merge($this->defaultClientOptions, $this->clientOptions));
        $js = "; $('#$id tbody').sortable($options);\n";
        $this->grid->view->registerJs($js, View::POS_READY, 'sortable-' . $this->grid->id);
    }
}
