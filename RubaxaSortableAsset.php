<?php

namespace azbuco\sortablewidgets;

use yii\web\AssetBundle;

class RubaxaSortableAsset extends AssetBundle {

    public $sourcePath = '@bower/sortablejs';
    public $js = [
        'Sortable.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
