<?php

namespace azbuco\sortablewidgets;

use yii\helpers\Json;
use yii\widgets\ListView;

class SortableListView extends ListView
{

    use SortableTrait;

    /**
     * @var array default configuration for sortable
     * $see https://github.com/RubaXa/Sortable
     * 
     */
    public $defaultClientOptions = [
        'animation' => 100,
        'draggable' => '.item',
        'dataIdAttr' => 'data-key',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (isset($this->options['class'])) {
            $this->options['class'] .= ' sortable-widget';
        } else {
            $this->options['class'] = 'sortable-widget';
        }

        if (isset($this->options['id'])) {
            $this->id = $this->options['id'];
        } else {
            $this->options['id'] = $this->id;
        }

        // defaults
        $this->setDefaults();

        // scripts
        $this->registerBundle();
        $this->registerClientScript();
    }

    public function registerClientScript()
    {
        $id = $this->id;
        $options = Json::encode($this->clientOptions);
        $js = "; $('#$id').sortable($options);\n";
        $this->view->registerJs($js);
    }

}
