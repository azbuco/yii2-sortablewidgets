<?php
/* @var $widget azbuco\sortablewidgets\SortableSelect */
/* @var $inline text */
/* @var $selected text */
/* @var $avialable text */
/* @var $title text|null */
?>
<div>
    <div class="input-group">
        <?= $inline ?>
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modal-<?= $widget->getId() ?>"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></button>
        </span>
    </div>
</div>
<div id="modal-<?= $widget->getId() ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= $title !== null ? $title : Yii::t('sortablewidgets', 'Arrange fields') ?></h4>
            </div>
            <div class="modal-body">
                <div class="sortable-select-interface">
                    <div class="sortable-select-selected">
                        <div class="sortable-select-label"><?= Yii::t('sortablewidgets', 'Selected fields') ?></div>
                        <?= $selected ?>
                    </div>
                    <div class="sortable-select-avialable">
                        <div class="sortable-select-label"><?= Yii::t('sortablewidgets', 'Avialable fields') ?></div>
                        <div class="sortable-select-items"><?= $avialable ?></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('sortablewidgets', 'Close') ?></button>
            </div>
        </div>
    </div>
</div> 
